/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.controller.exceptions.NonexistentEntityException;
import com.controller.exceptions.RollbackFailureException;
import com.entities.Mantenimientos;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.entities.Talleres;
import com.entities.Vehiculos;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author Kevin Lovos
 */
public class MantenimientosJpaController implements Serializable {

    public MantenimientosJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Mantenimientos mantenimientos) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Talleres idTaller = mantenimientos.getIdTaller();
            if (idTaller != null) {
                idTaller = em.getReference(idTaller.getClass(), idTaller.getId());
                mantenimientos.setIdTaller(idTaller);
            }
            Vehiculos idVehiculo = mantenimientos.getIdVehiculo();
            if (idVehiculo != null) {
                idVehiculo = em.getReference(idVehiculo.getClass(), idVehiculo.getId());
                mantenimientos.setIdVehiculo(idVehiculo);
            }
            em.persist(mantenimientos);
            if (idTaller != null) {
                idTaller.getMantenimientosList().add(mantenimientos);
                idTaller = em.merge(idTaller);
            }
            if (idVehiculo != null) {
                idVehiculo.getMantenimientosList().add(mantenimientos);
                idVehiculo = em.merge(idVehiculo);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Mantenimientos mantenimientos) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Mantenimientos persistentMantenimientos = em.find(Mantenimientos.class, mantenimientos.getId());
            Talleres idTallerOld = persistentMantenimientos.getIdTaller();
            Talleres idTallerNew = mantenimientos.getIdTaller();
            Vehiculos idVehiculoOld = persistentMantenimientos.getIdVehiculo();
            Vehiculos idVehiculoNew = mantenimientos.getIdVehiculo();
            if (idTallerNew != null) {
                idTallerNew = em.getReference(idTallerNew.getClass(), idTallerNew.getId());
                mantenimientos.setIdTaller(idTallerNew);
            }
            if (idVehiculoNew != null) {
                idVehiculoNew = em.getReference(idVehiculoNew.getClass(), idVehiculoNew.getId());
                mantenimientos.setIdVehiculo(idVehiculoNew);
            }
            mantenimientos = em.merge(mantenimientos);
            if (idTallerOld != null && !idTallerOld.equals(idTallerNew)) {
                idTallerOld.getMantenimientosList().remove(mantenimientos);
                idTallerOld = em.merge(idTallerOld);
            }
            if (idTallerNew != null && !idTallerNew.equals(idTallerOld)) {
                idTallerNew.getMantenimientosList().add(mantenimientos);
                idTallerNew = em.merge(idTallerNew);
            }
            if (idVehiculoOld != null && !idVehiculoOld.equals(idVehiculoNew)) {
                idVehiculoOld.getMantenimientosList().remove(mantenimientos);
                idVehiculoOld = em.merge(idVehiculoOld);
            }
            if (idVehiculoNew != null && !idVehiculoNew.equals(idVehiculoOld)) {
                idVehiculoNew.getMantenimientosList().add(mantenimientos);
                idVehiculoNew = em.merge(idVehiculoNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = mantenimientos.getId();
                if (findMantenimientos(id) == null) {
                    throw new NonexistentEntityException("The mantenimientos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Mantenimientos mantenimientos;
            try {
                mantenimientos = em.getReference(Mantenimientos.class, id);
                mantenimientos.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The mantenimientos with id " + id + " no longer exists.", enfe);
            }
            Talleres idTaller = mantenimientos.getIdTaller();
            if (idTaller != null) {
                idTaller.getMantenimientosList().remove(mantenimientos);
                idTaller = em.merge(idTaller);
            }
            Vehiculos idVehiculo = mantenimientos.getIdVehiculo();
            if (idVehiculo != null) {
                idVehiculo.getMantenimientosList().remove(mantenimientos);
                idVehiculo = em.merge(idVehiculo);
            }
            em.remove(mantenimientos);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Mantenimientos> findMantenimientosEntities() {
        return findMantenimientosEntities(true, -1, -1);
    }

    public List<Mantenimientos> findMantenimientosEntities(int maxResults, int firstResult) {
        return findMantenimientosEntities(false, maxResults, firstResult);
    }

    private List<Mantenimientos> findMantenimientosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Mantenimientos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Mantenimientos findMantenimientos(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Mantenimientos.class, id);
        } finally {
            em.close();
        }
    }

    public int getMantenimientosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Mantenimientos> rt = cq.from(Mantenimientos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
