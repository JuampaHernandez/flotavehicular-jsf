/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.controller.exceptions.IllegalOrphanException;
import com.controller.exceptions.NonexistentEntityException;
import com.controller.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.entities.Mantenimientos;
import com.entities.Talleres;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author Kevin Lovos
 */
public class TalleresJpaController implements Serializable {

    public TalleresJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Talleres talleres) throws RollbackFailureException, Exception {
        if (talleres.getMantenimientosList() == null) {
            talleres.setMantenimientosList(new ArrayList<Mantenimientos>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<Mantenimientos> attachedMantenimientosList = new ArrayList<Mantenimientos>();
            for (Mantenimientos mantenimientosListMantenimientosToAttach : talleres.getMantenimientosList()) {
                mantenimientosListMantenimientosToAttach = em.getReference(mantenimientosListMantenimientosToAttach.getClass(), mantenimientosListMantenimientosToAttach.getId());
                attachedMantenimientosList.add(mantenimientosListMantenimientosToAttach);
            }
            talleres.setMantenimientosList(attachedMantenimientosList);
            em.persist(talleres);
            for (Mantenimientos mantenimientosListMantenimientos : talleres.getMantenimientosList()) {
                Talleres oldIdTallerOfMantenimientosListMantenimientos = mantenimientosListMantenimientos.getIdTaller();
                mantenimientosListMantenimientos.setIdTaller(talleres);
                mantenimientosListMantenimientos = em.merge(mantenimientosListMantenimientos);
                if (oldIdTallerOfMantenimientosListMantenimientos != null) {
                    oldIdTallerOfMantenimientosListMantenimientos.getMantenimientosList().remove(mantenimientosListMantenimientos);
                    oldIdTallerOfMantenimientosListMantenimientos = em.merge(oldIdTallerOfMantenimientosListMantenimientos);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Talleres talleres) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Talleres persistentTalleres = em.find(Talleres.class, talleres.getId());
            List<Mantenimientos> mantenimientosListOld = persistentTalleres.getMantenimientosList();
            List<Mantenimientos> mantenimientosListNew = talleres.getMantenimientosList();
            List<String> illegalOrphanMessages = null;
            for (Mantenimientos mantenimientosListOldMantenimientos : mantenimientosListOld) {
                if (!mantenimientosListNew.contains(mantenimientosListOldMantenimientos)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Mantenimientos " + mantenimientosListOldMantenimientos + " since its idTaller field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Mantenimientos> attachedMantenimientosListNew = new ArrayList<Mantenimientos>();
            for (Mantenimientos mantenimientosListNewMantenimientosToAttach : mantenimientosListNew) {
                mantenimientosListNewMantenimientosToAttach = em.getReference(mantenimientosListNewMantenimientosToAttach.getClass(), mantenimientosListNewMantenimientosToAttach.getId());
                attachedMantenimientosListNew.add(mantenimientosListNewMantenimientosToAttach);
            }
            mantenimientosListNew = attachedMantenimientosListNew;
            talleres.setMantenimientosList(mantenimientosListNew);
            talleres = em.merge(talleres);
            for (Mantenimientos mantenimientosListNewMantenimientos : mantenimientosListNew) {
                if (!mantenimientosListOld.contains(mantenimientosListNewMantenimientos)) {
                    Talleres oldIdTallerOfMantenimientosListNewMantenimientos = mantenimientosListNewMantenimientos.getIdTaller();
                    mantenimientosListNewMantenimientos.setIdTaller(talleres);
                    mantenimientosListNewMantenimientos = em.merge(mantenimientosListNewMantenimientos);
                    if (oldIdTallerOfMantenimientosListNewMantenimientos != null && !oldIdTallerOfMantenimientosListNewMantenimientos.equals(talleres)) {
                        oldIdTallerOfMantenimientosListNewMantenimientos.getMantenimientosList().remove(mantenimientosListNewMantenimientos);
                        oldIdTallerOfMantenimientosListNewMantenimientos = em.merge(oldIdTallerOfMantenimientosListNewMantenimientos);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = talleres.getId();
                if (findTalleres(id) == null) {
                    throw new NonexistentEntityException("The talleres with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Talleres talleres;
            try {
                talleres = em.getReference(Talleres.class, id);
                talleres.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The talleres with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Mantenimientos> mantenimientosListOrphanCheck = talleres.getMantenimientosList();
            for (Mantenimientos mantenimientosListOrphanCheckMantenimientos : mantenimientosListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Talleres (" + talleres + ") cannot be destroyed since the Mantenimientos " + mantenimientosListOrphanCheckMantenimientos + " in its mantenimientosList field has a non-nullable idTaller field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(talleres);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Talleres> findTalleresEntities() {
        return findTalleresEntities(true, -1, -1);
    }

    public List<Talleres> findTalleresEntities(int maxResults, int firstResult) {
        return findTalleresEntities(false, maxResults, firstResult);
    }

    private List<Talleres> findTalleresEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Talleres.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Talleres findTalleres(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Talleres.class, id);
        } finally {
            em.close();
        }
    }

    public int getTalleresCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Talleres> rt = cq.from(Talleres.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
