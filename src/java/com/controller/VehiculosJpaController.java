/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.controller.exceptions.IllegalOrphanException;
import com.controller.exceptions.NonexistentEntityException;
import com.controller.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.entities.Mantenimientos;
import com.entities.Vehiculos;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author Kevin Lovos
 */
public class VehiculosJpaController implements Serializable {

    public VehiculosJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Vehiculos vehiculos) throws RollbackFailureException, Exception {
        if (vehiculos.getMantenimientosList() == null) {
            vehiculos.setMantenimientosList(new ArrayList<Mantenimientos>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<Mantenimientos> attachedMantenimientosList = new ArrayList<Mantenimientos>();
            for (Mantenimientos mantenimientosListMantenimientosToAttach : vehiculos.getMantenimientosList()) {
                mantenimientosListMantenimientosToAttach = em.getReference(mantenimientosListMantenimientosToAttach.getClass(), mantenimientosListMantenimientosToAttach.getId());
                attachedMantenimientosList.add(mantenimientosListMantenimientosToAttach);
            }
            vehiculos.setMantenimientosList(attachedMantenimientosList);
            em.persist(vehiculos);
            for (Mantenimientos mantenimientosListMantenimientos : vehiculos.getMantenimientosList()) {
                Vehiculos oldIdVehiculoOfMantenimientosListMantenimientos = mantenimientosListMantenimientos.getIdVehiculo();
                mantenimientosListMantenimientos.setIdVehiculo(vehiculos);
                mantenimientosListMantenimientos = em.merge(mantenimientosListMantenimientos);
                if (oldIdVehiculoOfMantenimientosListMantenimientos != null) {
                    oldIdVehiculoOfMantenimientosListMantenimientos.getMantenimientosList().remove(mantenimientosListMantenimientos);
                    oldIdVehiculoOfMantenimientosListMantenimientos = em.merge(oldIdVehiculoOfMantenimientosListMantenimientos);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Vehiculos vehiculos) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Vehiculos persistentVehiculos = em.find(Vehiculos.class, vehiculos.getId());
            List<Mantenimientos> mantenimientosListOld = persistentVehiculos.getMantenimientosList();
            List<Mantenimientos> mantenimientosListNew = vehiculos.getMantenimientosList();
            List<String> illegalOrphanMessages = null;
            for (Mantenimientos mantenimientosListOldMantenimientos : mantenimientosListOld) {
                if (!mantenimientosListNew.contains(mantenimientosListOldMantenimientos)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Mantenimientos " + mantenimientosListOldMantenimientos + " since its idVehiculo field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Mantenimientos> attachedMantenimientosListNew = new ArrayList<Mantenimientos>();
            for (Mantenimientos mantenimientosListNewMantenimientosToAttach : mantenimientosListNew) {
                mantenimientosListNewMantenimientosToAttach = em.getReference(mantenimientosListNewMantenimientosToAttach.getClass(), mantenimientosListNewMantenimientosToAttach.getId());
                attachedMantenimientosListNew.add(mantenimientosListNewMantenimientosToAttach);
            }
            mantenimientosListNew = attachedMantenimientosListNew;
            vehiculos.setMantenimientosList(mantenimientosListNew);
            vehiculos = em.merge(vehiculos);
            for (Mantenimientos mantenimientosListNewMantenimientos : mantenimientosListNew) {
                if (!mantenimientosListOld.contains(mantenimientosListNewMantenimientos)) {
                    Vehiculos oldIdVehiculoOfMantenimientosListNewMantenimientos = mantenimientosListNewMantenimientos.getIdVehiculo();
                    mantenimientosListNewMantenimientos.setIdVehiculo(vehiculos);
                    mantenimientosListNewMantenimientos = em.merge(mantenimientosListNewMantenimientos);
                    if (oldIdVehiculoOfMantenimientosListNewMantenimientos != null && !oldIdVehiculoOfMantenimientosListNewMantenimientos.equals(vehiculos)) {
                        oldIdVehiculoOfMantenimientosListNewMantenimientos.getMantenimientosList().remove(mantenimientosListNewMantenimientos);
                        oldIdVehiculoOfMantenimientosListNewMantenimientos = em.merge(oldIdVehiculoOfMantenimientosListNewMantenimientos);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = vehiculos.getId();
                if (findVehiculos(id) == null) {
                    throw new NonexistentEntityException("The vehiculos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Vehiculos vehiculos;
            try {
                vehiculos = em.getReference(Vehiculos.class, id);
                vehiculos.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The vehiculos with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Mantenimientos> mantenimientosListOrphanCheck = vehiculos.getMantenimientosList();
            for (Mantenimientos mantenimientosListOrphanCheckMantenimientos : mantenimientosListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Vehiculos (" + vehiculos + ") cannot be destroyed since the Mantenimientos " + mantenimientosListOrphanCheckMantenimientos + " in its mantenimientosList field has a non-nullable idVehiculo field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(vehiculos);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Vehiculos> findVehiculosEntities() {
        return findVehiculosEntities(true, -1, -1);
    }

    public List<Vehiculos> findVehiculosEntities(int maxResults, int firstResult) {
        return findVehiculosEntities(false, maxResults, firstResult);
    }

    private List<Vehiculos> findVehiculosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Vehiculos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Vehiculos findVehiculos(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Vehiculos.class, id);
        } finally {
            em.close();
        }
    }

    public int getVehiculosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Vehiculos> rt = cq.from(Vehiculos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
