/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kevin Lovos
 */
@Entity
@Table(name = "mantenimientos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mantenimientos.findAll", query = "SELECT m FROM Mantenimientos m")
    , @NamedQuery(name = "Mantenimientos.findById", query = "SELECT m FROM Mantenimientos m WHERE m.id = :id")
    , @NamedQuery(name = "Mantenimientos.findByTipoMantenimiento", query = "SELECT m FROM Mantenimientos m WHERE m.tipoMantenimiento = :tipoMantenimiento")
    , @NamedQuery(name = "Mantenimientos.findByFechaEntrada", query = "SELECT m FROM Mantenimientos m WHERE m.fechaEntrada = :fechaEntrada")
    , @NamedQuery(name = "Mantenimientos.findByFechaSalida", query = "SELECT m FROM Mantenimientos m WHERE m.fechaSalida = :fechaSalida")
    , @NamedQuery(name = "Mantenimientos.findByFallaPrincipal", query = "SELECT m FROM Mantenimientos m WHERE m.fallaPrincipal = :fallaPrincipal")
    , @NamedQuery(name = "Mantenimientos.findByFallaSecundaria", query = "SELECT m FROM Mantenimientos m WHERE m.fallaSecundaria = :fallaSecundaria")
    , @NamedQuery(name = "Mantenimientos.findByEstado", query = "SELECT m FROM Mantenimientos m WHERE m.estado = :estado")})
public class Mantenimientos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "tipoMantenimiento")
    private String tipoMantenimiento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaEntrada")
    @Temporal(TemporalType.DATE)
    private Date fechaEntrada;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaSalida")
    @Temporal(TemporalType.DATE)
    private Date fechaSalida;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "fallaPrincipal")
    private String fallaPrincipal;
    @Size(max = 50)
    @Column(name = "fallaSecundaria")
    private String fallaSecundaria;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private int estado;
    @JoinColumn(name = "idTaller", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Talleres idTaller;
    @JoinColumn(name = "idVehiculo", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Vehiculos idVehiculo;

    public Mantenimientos() {
    }

    public Mantenimientos(Integer id) {
        this.id = id;
    }

    public Mantenimientos(Integer id, String tipoMantenimiento, Date fechaEntrada, Date fechaSalida, String fallaPrincipal, int estado) {
        this.id = id;
        this.tipoMantenimiento = tipoMantenimiento;
        this.fechaEntrada = fechaEntrada;
        this.fechaSalida = fechaSalida;
        this.fallaPrincipal = fallaPrincipal;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipoMantenimiento() {
        return tipoMantenimiento;
    }

    public void setTipoMantenimiento(String tipoMantenimiento) {
        this.tipoMantenimiento = tipoMantenimiento;
    }

    public Date getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(Date fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getFallaPrincipal() {
        return fallaPrincipal;
    }

    public void setFallaPrincipal(String fallaPrincipal) {
        this.fallaPrincipal = fallaPrincipal;
    }

    public String getFallaSecundaria() {
        return fallaSecundaria;
    }

    public void setFallaSecundaria(String fallaSecundaria) {
        this.fallaSecundaria = fallaSecundaria;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = 1;
    }

    public Talleres getIdTaller() {
        return idTaller;
    }

    public void setIdTaller(Talleres idTaller) {
        this.idTaller = idTaller;
    }

    public Vehiculos getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(Vehiculos idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mantenimientos)) {
            return false;
        }
        Mantenimientos other = (Mantenimientos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entities.Mantenimientos[ id=" + id + " ]";
    }
    
}
