/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kevin Lovos
 */
@Entity
@Table(name = "vehiculos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vehiculos.findAll", query = "SELECT v FROM Vehiculos v")
    , @NamedQuery(name = "Vehiculos.findById", query = "SELECT v FROM Vehiculos v WHERE v.id = :id")
    , @NamedQuery(name = "Vehiculos.findByFechaAdquisicion", query = "SELECT v FROM Vehiculos v WHERE v.fechaAdquisicion = :fechaAdquisicion")
    , @NamedQuery(name = "Vehiculos.findByTaller", query = "SELECT v FROM Vehiculos v WHERE v.taller = :taller")
    , @NamedQuery(name = "Vehiculos.findByModelo", query = "SELECT v FROM Vehiculos v WHERE v.modelo = :modelo")
    , @NamedQuery(name = "Vehiculos.findByPlaca", query = "SELECT v FROM Vehiculos v WHERE v.placa = :placa")
    , @NamedQuery(name = "Vehiculos.findByMarca", query = "SELECT v FROM Vehiculos v WHERE v.marca = :marca")
    , @NamedQuery(name = "Vehiculos.findByKilometrosRecorridos", query = "SELECT v FROM Vehiculos v WHERE v.kilometrosRecorridos = :kilometrosRecorridos")
    , @NamedQuery(name = "Vehiculos.findByFechaUltimoCambioAceite", query = "SELECT v FROM Vehiculos v WHERE v.fechaUltimoCambioAceite = :fechaUltimoCambioAceite")
    , @NamedQuery(name = "Vehiculos.findByFechaUltimoMantenimiento", query = "SELECT v FROM Vehiculos v WHERE v.fechaUltimoMantenimiento = :fechaUltimoMantenimiento")
    , @NamedQuery(name = "Vehiculos.findByObservaciones", query = "SELECT v FROM Vehiculos v WHERE v.observaciones = :observaciones")
    , @NamedQuery(name = "Vehiculos.findByEstado", query = "SELECT v FROM Vehiculos v WHERE v.estado = :estado")})
public class Vehiculos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "fechaAdquisicion")
    private String fechaAdquisicion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taller")
    private boolean taller;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "modelo")
    private String modelo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "placa")
    private String placa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "marca")
    private String marca;
    @Basic(optional = false)
    @NotNull
    @Column(name = "kilometrosRecorridos")
    private double kilometrosRecorridos;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "fechaUltimoCambioAceite")
    private String fechaUltimoCambioAceite;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "fechaUltimoMantenimiento")
    private String fechaUltimoMantenimiento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "observaciones")
    private String observaciones;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private int estado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idVehiculo")
    private List<Mantenimientos> mantenimientosList;

    public Vehiculos() {
    }

    public Vehiculos(Integer id) {
        this.id = id;
    }

    public Vehiculos(Integer id, String fechaAdquisicion, boolean taller, String modelo, String placa, String marca, double kilometrosRecorridos, String fechaUltimoCambioAceite, String fechaUltimoMantenimiento, String observaciones, int estado) {
        this.id = id;
        this.fechaAdquisicion = fechaAdquisicion;
        this.taller = taller;
        this.modelo = modelo;
        this.placa = placa;
        this.marca = marca;
        this.kilometrosRecorridos = kilometrosRecorridos;
        this.fechaUltimoCambioAceite = fechaUltimoCambioAceite;
        this.fechaUltimoMantenimiento = fechaUltimoMantenimiento;
        this.observaciones = observaciones;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFechaAdquisicion() {
        return fechaAdquisicion;
    }

    public void setFechaAdquisicion(String fechaAdquisicion) {
        this.fechaAdquisicion = fechaAdquisicion;
    }

    public boolean getTaller() {
        return taller;
    }

    public void setTaller(boolean taller) {
        this.taller = taller;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public double getKilometrosRecorridos() {
        return kilometrosRecorridos;
    }

    public void setKilometrosRecorridos(double kilometrosRecorridos) {
        this.kilometrosRecorridos = kilometrosRecorridos;
    }

    public String getFechaUltimoCambioAceite() {
        return fechaUltimoCambioAceite;
    }

    public void setFechaUltimoCambioAceite(String fechaUltimoCambioAceite) {
        this.fechaUltimoCambioAceite = fechaUltimoCambioAceite;
    }

    public String getFechaUltimoMantenimiento() {
        return fechaUltimoMantenimiento;
    }

    public void setFechaUltimoMantenimiento(String fechaUltimoMantenimiento) {
        this.fechaUltimoMantenimiento = fechaUltimoMantenimiento;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<Mantenimientos> getMantenimientosList() {
        return mantenimientosList;
    }

    public void setMantenimientosList(List<Mantenimientos> mantenimientosList) {
        this.mantenimientosList = mantenimientosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vehiculos)) {
            return false;
        }
        Vehiculos other = (Vehiculos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return placa;
    }
    
}
